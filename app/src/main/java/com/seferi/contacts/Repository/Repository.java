package com.seferi.contacts.Repository;

import com.seferi.contacts.Contact;

import java.util.List;

public interface Repository {
   void saveContact(Contact input) throws Exception;
   List<Contact> searchContact(String name);
   void sortContacts(List<Contact> contacts) throws Exception;
}