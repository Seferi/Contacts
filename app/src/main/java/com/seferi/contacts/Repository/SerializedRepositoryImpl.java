package com.seferi.contacts.Repository;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.seferi.contacts.Contact;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SerializedRepositoryImpl implements Repository {

    private static final String ALL_CONTACTS_KEY = "all_contacts";
    private SharedPreferences sharedPreferences;
    private static SerializedRepositoryImpl instance;

    private SerializedRepositoryImpl(Context context) {
        sharedPreferences = context.getSharedPreferences("alternate_db", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();

        if (null == getAllContacts()) {
            editor.putString(ALL_CONTACTS_KEY, gson.toJson(new ArrayList<Contact>()));
            editor.commit();
        }
    }

    public ArrayList<Contact> getAllContacts() {
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<Contact>>(){}.getType();
        ArrayList<Contact> contacts = gson.fromJson(sharedPreferences.getString(ALL_CONTACTS_KEY, null), type);
        return contacts;
    }

    public static SerializedRepositoryImpl getInstance(Context context) {
        if (null == instance) {
            instance = new SerializedRepositoryImpl(context);
        }
        return instance;
    }

    public Contact getContactById(String id) {
        ArrayList<Contact> contacts = getAllContacts();
        if (null != contacts) {
            for (Contact c : contacts) {
                if (c.getId().equals(id)) {
                    return c;
                }
            }
        }
        return null;
    }

    public boolean addToAllContacts(Contact contact) {
        Log.d("TAG", contact.getName() + " " + contact.getSurname());

        ArrayList<Contact> contacts = getAllContacts();
        if (null != contacts) {
            if (contacts.add(contact)) {
                Gson gson = new Gson();
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.remove(ALL_CONTACTS_KEY);
                editor.putString(ALL_CONTACTS_KEY, gson.toJson(contacts));
                editor.commit();
                return true;
            }
        }
        return false;
    }

    public boolean removeFromAllContacts(Contact contact) {
        ArrayList<Contact> contacts = getAllContacts();
        if (null != contacts) {
            for (Contact c : contacts) {
                if (c.getId().equals(contact.getId())) {
                    if (contacts.remove(c)) {
                        Gson gson = new Gson();
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.remove(ALL_CONTACTS_KEY);
                        editor.putString(ALL_CONTACTS_KEY, gson.toJson(contacts));
                        editor.commit();
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public void saveContact(Contact input) throws Exception {

    }

    @Override
    public List<Contact> searchContact(String name) {
        return null;
    }

    @Override
    public void sortContacts(List<Contact> contacts) throws Exception {

    }
}

