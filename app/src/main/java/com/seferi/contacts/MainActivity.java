package com.seferi.contacts;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.seferi.contacts.Repository.SerializedRepositoryImpl;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView contactsRecView;
    private FloatingActionButton btnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btnAdd = findViewById(R.id.btnFloatingAdd);

        contactsRecView = findViewById(R.id.contactsRecView);
        SerializedRepositoryImpl repository = SerializedRepositoryImpl.getInstance(this);



        //repository.addToAllContacts(new Contact("1", "Jack", "Hop", "23424", "asdasd", "asdasd", "sadasd", "asdasd", "asdasd", "sadasd"));
        //repository.addToAllContacts(new Contact("2", "Robert", "jackal", "23424", "asdasd", "asdasd", "sadasd", "asdasd", "asdasd", "sadasd"));
        //repository.addToAllContacts(new Contact("3", "Pete", "VBaals", "23424", "asdasd", "asdasd", "sadasd", "asdasd", "asdasd", "sadasd"));

        ArrayList<Contact> contacts = repository.getAllContacts();
        ContactsRecViewAdapter adapter = new ContactsRecViewAdapter(this);
        adapter.setContacts(contacts);

        contactsRecView.setAdapter(adapter);
        contactsRecView.setLayoutManager(new LinearLayoutManager(this));


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NewContactFormActivity.class);
                startActivity(intent);
            }
        });
    }

}