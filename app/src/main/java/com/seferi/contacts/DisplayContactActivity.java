package com.seferi.contacts;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.seferi.contacts.Repository.SerializedRepositoryImpl;

import java.util.UUID;

public class DisplayContactActivity extends AppCompatActivity {

    private TextInputEditText edtTxtName, edtTxtSurname, edtTxtEmail, edtTxtTelephone, edtTxtStreet, edtTxtStreetNr,
            edtTxtZipCode, edtTxtCity, edtTxtCountry;

    private TextInputLayout txtLayoutName, txtLayoutSurname, txtLayoutEmail, txtLayoutTelephone, txtLayoutStreet,
            txtLayoutStreetNr, txtLayoutCity, txtLayoutZipCode, txtLayoutCountry;

    private String name, surname, email, telephone, street, streetNr, city, zipCode, country;

    private Button btnDelete, btnEdit, btnSave;

    private Contact contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_contact);

        initViews();

        Intent intent = getIntent();
        String contactId = intent.getStringExtra("contactId");
        contact = (SerializedRepositoryImpl.getInstance(this).getContactById(contactId));

        edtTxtName.setText(contact.getName());
        edtTxtSurname.setText(contact.getSurname());
        edtTxtEmail.setText(contact.getEmail());
        edtTxtTelephone.setText(contact.getTelephoneNumber());
        edtTxtStreet.setText(contact.getStreet());
        edtTxtStreetNr.setText(contact.getHouseNumber());
        edtTxtZipCode.setText(contact.getPostalCode());
        edtTxtCity.setText(contact.getCity());
        edtTxtCountry.setText(contact.getCountry());

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(DisplayContactActivity.this);
                builder.setMessage("Are you sure you want to delete " + contact.getName() + " " + contact.getSurname() + " ?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (SerializedRepositoryImpl.getInstance(DisplayContactActivity.this).removeFromAllContacts(contact)) {
                            Toast.makeText(DisplayContactActivity.this, "Contact deleted", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(DisplayContactActivity.this, MainActivity.class);
                            startActivity(intent);
                        }
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.create().show();
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnEdit.setText(getString(R.string.btnTextSave));
                edtTxtName.setEnabled(true);
                edtTxtSurname.setEnabled(true);
                edtTxtEmail.setEnabled(true);
                edtTxtTelephone.setEnabled(true);
                edtTxtStreet.setEnabled(true);
                edtTxtStreetNr.setEnabled(true);
                edtTxtZipCode.setEnabled(true);
                edtTxtCity.setEnabled(true);
                edtTxtCountry.setEnabled(true);
                btnEdit.setVisibility(View.GONE);
                btnSave.setVisibility(View.VISIBLE);
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initData();
                Contact editedContact = new Contact(contact.getId(), name, surname, telephone, email, street, streetNr, zipCode, city, country);
                SerializedRepositoryImpl.getInstance(DisplayContactActivity.this).removeFromAllContacts(contact);
                SerializedRepositoryImpl.getInstance(DisplayContactActivity.this).addToAllContacts(editedContact);
                Toast.makeText(DisplayContactActivity.this, "Contact saved",Toast.LENGTH_LONG).show();
                Intent intent = new Intent(DisplayContactActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });


    }

    private void initViews() {
        edtTxtName = findViewById(R.id.edtTxtName);
        edtTxtSurname = findViewById(R.id.edtTxtSurname);
        edtTxtEmail = findViewById(R.id.edtTxtEmail);
        edtTxtTelephone = findViewById(R.id.edtTxtTelephone);
        edtTxtStreet = findViewById(R.id.edtTxtStreet);
        edtTxtStreetNr = findViewById(R.id.edtTxtStreetNr);
        edtTxtZipCode = findViewById(R.id.edtTxtZipCode);
        edtTxtCity = findViewById(R.id.edtTxtCity);
        edtTxtCountry = findViewById(R.id.edtTxtCountry);

        txtLayoutName = findViewById(R.id.nameTextField);
        txtLayoutSurname = findViewById(R.id.surnameTextField);
        txtLayoutEmail = findViewById(R.id.emailTextField);
        txtLayoutTelephone = findViewById(R.id.telephoneTextField);
        txtLayoutStreet = findViewById(R.id.streetTextField);
        txtLayoutStreetNr = findViewById(R.id.streetNrTextField);
        txtLayoutZipCode = findViewById(R.id.zipCodeTextField);
        txtLayoutCity = findViewById(R.id.cityTextField);
        txtLayoutCountry = findViewById(R.id.countryTextField);

        btnDelete = findViewById(R.id.btnDelete);
        btnEdit = findViewById(R.id.btnEdit);
        btnSave = findViewById(R.id.btnSave);
    }

    private void initData() {
        name = txtLayoutName.getEditText().getText().toString();
        surname = txtLayoutSurname.getEditText().getText().toString();
        email = txtLayoutEmail.getEditText().getText().toString();
        telephone = txtLayoutTelephone.getEditText().getText().toString();
        street = txtLayoutStreet.getEditText().getText().toString();
        streetNr = txtLayoutStreetNr.getEditText().getText().toString();
        city = txtLayoutCity.getEditText().getText().toString();
        zipCode = txtLayoutZipCode.getEditText().getText().toString();
        country = txtLayoutCountry.getEditText().getText().toString();
    }
}