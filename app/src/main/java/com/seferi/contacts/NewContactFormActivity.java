package com.seferi.contacts;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.seferi.contacts.Repository.SerializedRepositoryImpl;

import java.util.UUID;

public class NewContactFormActivity extends AppCompatActivity {

    private TextInputLayout edtTxtName, edtTxtSurname, edtTxtEmail, edtTxtTelephone, edtTxtStreet,
            edtTxtStreetNr, edtTxtCity, edtTxtZipCode, edtTxtCountry;

    private Button btnSave;

    private String name, surname, email, telephone, street, streetNr, city, zipCode, country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_contact_form);

        initViews();

        Intent intent = getIntent();

        if (null != intent) {
            handleAddNewContact();
        }
    }


    private void handleAddNewContact() {
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initData();
                final String contactId = UUID.randomUUID().toString();
                Contact contact = new Contact(contactId, name, surname, telephone, email, street, streetNr, zipCode, city, country);
                if (SerializedRepositoryImpl.getInstance(NewContactFormActivity.this).addToAllContacts(contact)) {
                    Toast.makeText(NewContactFormActivity.this, "Contact added!",Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(NewContactFormActivity.this, MainActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(NewContactFormActivity.this, "Something went wrong, please try again!",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void initViews() {
        edtTxtName = findViewById(R.id.nameTextField);
        edtTxtSurname = findViewById(R.id.surnameTextField);
        edtTxtEmail = findViewById(R.id.emailTextField);
        edtTxtTelephone = findViewById(R.id.telephoneTextField);
        edtTxtStreet = findViewById(R.id.streetTextField);
        edtTxtStreetNr = findViewById(R.id.streetNrTextField);
        edtTxtCity = findViewById(R.id.cityTextField);
        edtTxtZipCode = findViewById(R.id.zipCodeTextField);
        edtTxtCountry = findViewById(R.id.countryTextField);
        btnSave = findViewById(R.id.btnSave);
    }

    private void initData() {
        name = edtTxtName.getEditText().getText().toString();
        surname = edtTxtSurname.getEditText().getText().toString();
        email = edtTxtEmail.getEditText().getText().toString();
        telephone = edtTxtTelephone.getEditText().getText().toString();
        street = edtTxtStreet.getEditText().getText().toString();
        streetNr = edtTxtStreetNr.getEditText().getText().toString();
        city = edtTxtCity.getEditText().getText().toString();
        zipCode = edtTxtZipCode.getEditText().getText().toString();
        country = edtTxtCountry.getEditText().getText().toString();
    }
}